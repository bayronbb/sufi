import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: "hazlo-realidad/hazlo-realidad",
    pathMatch: "full"
  },
  {
    path: 'hazlo-realidad/hazlo-realidad',
    loadChildren: './modules/make-it/make-it.module'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }