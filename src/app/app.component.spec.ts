import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import * as AOS from 'aos';

// Components
import { AppComponent } from './app.component';

import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { MakeItModule } from './modules/make-it/make-it.module';
import { BlogModule } from './modules/blog/blog.module';
import { ExplorePossibilitiesModule } from './modules/explore-possibilities/explore-possibilities.module';
import { AppRoutingModule } from './app-routing.module';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';

import { HeaderComponent } from './core/header/header.component';
import { BreadcrumbComponent } from './core/breadcrumb/breadcrumb.component';
import { FooterComponent } from './core/footer/footer.component';
import { TabsMobileComponent } from './core/tabs-mobile/tabs-mobile.component';

//Providers
import { WindowRef } from './core/providers/window-ref/window-ref';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        CoreModule,
        SharedModule,
        MakeItModule,
        BlogModule,
        ExplorePossibilitiesModule,
        AppRoutingModule,
        NgxPageScrollCoreModule.forRoot({duration: 800})
      ],
      declarations: [
        AppComponent
      ],
      providers: [WindowRef]
    }).compileComponents();
  }));

  afterEach(() => {

  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
  it('should load AOS', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;

    app.ngOnInit();
    expect(AOS.init()).toBeTruthy();
  });



});
