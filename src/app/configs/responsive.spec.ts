import { Responsive } from './responsive';


describe('Reponsive test', ()=>{
    let responsive : Responsive;


    beforeEach (() =>{
        responsive = new Responsive();
      });
    
    it('less than',()=>{
        expect (responsive.mqMin('12','13')).toBeFalsy();
        expect (responsive.mqMin('12','12')).toBeTruthy();
        expect (responsive.mqMin('12','11')).toBeTruthy();
    });

    it('greater than',()=>{
        expect (responsive.mqMax('12','13')).toBeTruthy();
        expect (responsive.mqMax('12','12')).toBeFalsy();
        expect (responsive.mqMax('12','11')).toBeFalsy();
    });

    it('between',()=>{
        expect (responsive.mqMinMax('12','11','13')).toBeTruthy();
        expect (responsive.mqMinMax('10','11','13')).toBeFalsy();
        expect (responsive.mqMinMax('14','11','13')).toBeFalsy();
    });

});