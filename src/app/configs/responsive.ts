export class Responsive {

    constructor () {
    }

    mqMin( evt, width ) {
        if ( evt >= width ) {
            return true
        } else {
            return false
        }
    }
    mqMax (evt, width) {
        if ( evt < width ) {
            return true
        } else {
            return false
        }
    }
    mqMinMax (evt, min, max) {
        if ( evt > min && evt < max) {
            return true;
        } else {
            return false;
        }
    }

}