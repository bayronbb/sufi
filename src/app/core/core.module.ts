import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

//Components
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { TabsMobileComponent } from './tabs-mobile/tabs-mobile.component';

//Provider
import { WindowRef } from './providers/window-ref/window-ref';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, BreadcrumbComponent, TabsMobileComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [WindowRef],
  exports: [HeaderComponent, FooterComponent, BreadcrumbComponent]
})
export class CoreModule { }
