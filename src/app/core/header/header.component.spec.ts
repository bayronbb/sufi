import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';
import { HeaderComponent } from './header.component';


describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [WindowRef]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('click nav', () => {

    component.clickNavMain('');
    expect(component.clickNavMain).toBeDefined();
    var event = { target: component.navMain.nativeElement };
    var activeMenu = spyOn(component,'activeMenu');
    component.clickNavMain(event)
    expect(activeMenu).toHaveBeenCalled();

  });

  it('active nav', () => {
    var e = jasmine.createSpyObj('e', ['preventDefault']);
    component.activeMenu(e);
    expect(e.preventDefault).toHaveBeenCalled();
    component.win.nativeWindow.innerWidth = 766;
    component.activeMenu(e);
    expect(component.isActive).toBeTruthy();
  });

});
