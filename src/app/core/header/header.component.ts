import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterViewInit {

  public isActive: Boolean = false;

  @ViewChild("navMain", { static: true}) navMain: ElementRef;

  constructor(public win: WindowRef) { }


  //Mehtods
  clickNavMain (e) {
    if( e.target !== this.navMain.nativeElement ) {
      return;
    }
    this.activeMenu(e);
  }

  activeMenu (e) {
    e.preventDefault();
    let widthWin =  this.win.nativeWindow.innerWidth;
    console.log(widthWin)
    if ( widthWin < 768) {
      this.isActive = !this.isActive;
      console.log("enter")
    }
  }

  //life cycles
  ngOnInit() { }

  ngAfterViewInit() {}

}
