const MockRequestSimulator = {
    data: {
        loanInfo: {
            businessLine: 'b4c010mb1a',
            term: 60,
            monthlyFeeAmount: 60,
            financingValueAmoun: 250000,
            dowPaymentAmount: 250000.00,
        },
        simulationDetail: {
            type: 'MONTO_FINANCIAR',
        }
    }
};
const MockResponseSimulator = {
    meta: {
        description: 'Non-standard meta-information that can not be represented realtion with data',
        _messageId: 'c4e6bd04-5149-11e7-b114-b2f933d5fe66',
        _version: '1.0',
        _requestDate: '2017-01-24T05:00:00.000Z',
        _responseSize: 1,
        _clientRequest: 'acxff62e-6f12-42de-9012-3e7304418abd'
    },
    data: {
        id: 0,
        simulation: {
            businessLine: 'b4c010mb1a',
            financingPlan: {
                planName: 'Educación',
                desc: '2500000',
                term: 60,
                detail: {
                    amountCode: '250000',
                    amount: 2500000.00,
                    amountDesc: 'Crédito de educación'
                },
                rates: {
                    desc: 'Descripción de la tasa aplicada en a simulación del plan',
                    value: 2500000.00
                }
            }
        }
    },
    links: {
        topLevelLinks: {
            description: 'www.bancolombia.com.co',
            self: 'www.bancolombia.com.co',
            related: 'www.bancolombia.com.co'
        }
    }
};

export { MockRequestSimulator, MockResponseSimulator };