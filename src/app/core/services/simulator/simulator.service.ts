import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MockRequestSimulator, MockResponseSimulator } from '../../mocks/mock-simulator';
import { ResponseSimulator } from '../../models/responseSimulator';
import { RequestSimulator } from '../../models/requestSimulator';


@Injectable({
  providedIn: 'root'
})
export class SimulatorService {

  constructor( private _http: HttpClient ) { 

  }

  simulatorHttp():ResponseSimulator {
    return MockResponseSimulator;
  }
}
