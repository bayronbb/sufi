import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import { HttpClient } from '@angular/common/http';
import { SimulatorService } from './simulator.service';
import { MockResponseSimulator } from '../../mocks/mock-simulator';
import { ResponseSimulator } from '../../models/responseSimulator';

describe('SimulatorService', () => {
  let service: SimulatorService;
  let httpTestController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
  }));

  it('should be created', () => {
    const service: SimulatorService = TestBed.get(SimulatorService);
    expect(service).toBeTruthy();
  });

  it('Should return response simulator', () => {
    service = TestBed.get(SimulatorService);
    let resp = service.simulatorHttp();
    expect(resp).toBe(MockResponseSimulator);
  })
});
