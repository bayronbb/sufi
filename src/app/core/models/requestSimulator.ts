export interface RequestSimulator {
    data: {
        loanInfo: {
            businessLine: string,
            term: number,
            monthlyFeeAmount: number,
            financingValueAmoun: number,
            dowPaymentAmount: number,
        },
        simulationDetail: {
            type: string
        }
    }
}