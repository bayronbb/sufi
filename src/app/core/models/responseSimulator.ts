export interface ResponseSimulator {
    meta: {
        description: string,
        _messageId: string;
        _version: string,
        _requestDate: string,
        _responseSize: number,
        _clientRequest: string
    },
    data: {
        id: number,
        simulation: {
            businessLine: string,
            financingPlan: {
                planName: string,
                desc: string,
                term: number,
                detail: {
                    amountCode: string,
                    amount: number,
                    amountDesc: string
                },
                rates: {
                    desc: string,
                    value: number
                }
            }
        }
    },
    links: {
        topLevelLinks: {
            description: string,
            self: string,
            related: string
        }
    }
};