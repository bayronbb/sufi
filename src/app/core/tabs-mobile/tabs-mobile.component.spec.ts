import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsMobileComponent } from './tabs-mobile.component';

describe('TabsMobileComponent', () => {
  let component: TabsMobileComponent;
  let fixture: ComponentFixture<TabsMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
