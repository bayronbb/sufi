import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WindowRef } from '../providers/window-ref/window-ref';
import { CommonModule } from '@angular/common';

//Components
import { HeaderComponent } from 'src/app/core/header/header.component';
import { FooterComponent } from 'src/app/core/footer/footer.component';
import { BreadcrumbComponent } from 'src/app/core/breadcrumb/breadcrumb.component';
import { TabsMobileComponent } from 'src/app/core/tabs-mobile/tabs-mobile.component';
import { Responsive } from 'src/app/configs/responsive';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let responsive: Responsive;

  let event = {
    target: {
      innerWidth: 768
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent, FooterComponent, BreadcrumbComponent, TabsMobileComponent ],
      imports:[
        CommonModule
      ],
      providers:[WindowRef]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    responsive = new Responsive();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Desktop should be true', () => {
    component.ngOnInit()
    fixture.detectChanges();
    component.onResize(event);
    fixture.detectChanges();
    //component.desktop = responsive.mqMin(event.target.innerWidth, 768);
    expect(component.desktop).toBeTruthy();
  });
});
