import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-title-main',
  template: `<div class="container">
  <div class="title-main">
      <h1 class="text-uppercase"><ng-content></ng-content></h1>
  </div>
</div>`,
  styleUrls: ['./title-main.component.scss']
})
export class TitleMainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
