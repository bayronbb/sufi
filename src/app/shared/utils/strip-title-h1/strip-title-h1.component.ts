import { Component, OnInit, Input, ContentChild, ViewChild } from '@angular/core';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-strip-title-h1',
  template: '<h1 class="strip-title bg-red {{ addClass }}"><ng-content></ng-content></h1>',
})
export class StripTitleH1Component implements OnInit {

  @Input() addClass;
  @Input() title;

  constructor() { }

  ngOnInit() {
  }

}
