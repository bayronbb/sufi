import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-strip-title-h2',
  template: '<h2 class="strip-title bg-red {{ addClass }}"><ng-content></ng-content></h2>',
})
export class StripTitleH2Component implements OnInit {

  @Input() addClass;

  constructor() { }

  ngOnInit() {
  }

}
