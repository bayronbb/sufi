import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-strip-title',
  template: `<h3 class="strip-title bg-red {{ addClass }}"><ng-content></ng-content></h3>`,
})
export class StripTitleComponent implements OnInit {

  @Input() addClass;

  constructor() { }

  ngOnInit() {
  }

}
