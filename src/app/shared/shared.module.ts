import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { TabsComponent } from './components/tabs/tabs.component';
import { StripTitleComponent } from './utils/strip-title/strip-title.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { AccordionItemComponent } from './components/accordion/accordion-item.component';
import { TabItemComponent } from './components/tabs/tab-item.component';
import { StripTitleH1Component } from './utils/strip-title-h1/strip-title-h1.component';
import { StripTitleH2Component } from './utils/strip-title-h2/strip-title-h2.component';
import { ItemCreditComponent } from './components/item-credit/item-credit.component';
import { NgbModalContactComponent } from './components/ngb-modal-contact/ngb-modal-contact.component';
import { SvgCloseComponent } from './utils/svg-close/svg-close.component';
import { TitleMainComponent } from './utils/title-main/title-main.component';

// Pipes
import { SpaceTitlePipe } from './pipes/space-title/space-title.pipe';
import { SafePipe } from './pipes/safe/safe.pipe';

@NgModule({
  declarations: [
    TabsComponent,
    TabItemComponent,
    StripTitleComponent,
    AccordionComponent,
    AccordionItemComponent,
    StripTitleH1Component,
    StripTitleH2Component,
    ItemCreditComponent,
    NgbModalContactComponent,
    SvgCloseComponent,
    SpaceTitlePipe,
    SafePipe,
    TitleMainComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TabsComponent,
    TabItemComponent,
    StripTitleComponent,
    AccordionComponent,
    AccordionItemComponent,
    StripTitleH1Component,
    StripTitleH2Component,
    ItemCreditComponent,
    NgbModalContactComponent,
    SvgCloseComponent,
    TitleMainComponent,
    SpaceTitlePipe,
    SafePipe
  ]
})
export class SharedModule { }
