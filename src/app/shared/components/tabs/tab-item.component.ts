import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tab-item',
  template: `<div [ngClass]="{'is-active': isActive}" class="tabs__cont">
            <ng-content></ng-content>
        </div>`,
  styleUrls: ['./tabs.component.scss']
})
export class TabItemComponent implements OnInit {
  @Input() title: String;
  @Input() isActive = false;

  constructor() { }

  ngOnInit() {
  }

}
