import { Component, OnInit, AfterContentInit, ContentChildren, QueryList } from '@angular/core';
import { TabItemComponent } from './tab-item.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements AfterContentInit {

  @ContentChildren(TabItemComponent) tabs: QueryList<TabItemComponent>;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit() {

    console.log(this.tabs)
    let activeTabs = this.tabs.filter((tab)=> tab.isActive );
    
    // if there is no active tab set, activate the first
    if(activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }

  selectTab(tab){
    // deactivate all tabs
    this.tabs.toArray().forEach(tab => tab.isActive = false);
    
    // activate the tab the user has clicked on.
    tab.isActive = true;
  }

}
