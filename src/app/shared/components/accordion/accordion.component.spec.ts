import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { QueryList, DebugElement, NO_ERRORS_SCHEMA, Component, CUSTOM_ELEMENTS_SCHEMA, EventEmitter } from '@angular/core';
import { By } from '@angular/platform-browser';

import { AccordionComponent } from './accordion.component';
import { AccordionItemComponent } from './accordion-item.component';
import { TabsComponent } from '../tabs/tabs.component';
import { TabItemComponent } from '../tabs/tab-item.component';
import { StripTitleComponent } from '../../utils/strip-title/strip-title.component';
import { StripTitleH1Component } from '../../utils/strip-title-h1/strip-title-h1.component';
import { StripTitleH2Component } from '../../utils/strip-title-h2/strip-title-h2.component';

describe('AccordionComponent', () => {
  let component: AccordionComponent;
  let componentItems: AccordionItemComponent;
  let fixture: ComponentFixture<AccordionComponent>;
  let fixtureItems: ComponentFixture<AccordionItemComponent>;
  
  let items = {
      title: "Tab 1",
      opened: false,
      toggle: new EventEmitter()
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TabsComponent,
        TabItemComponent,
        StripTitleComponent,
        AccordionComponent,
        AccordionItemComponent,
        StripTitleH1Component,
        StripTitleH2Component
      ],
      imports: [
        CommonModule
      ],
        schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(()  => {

    fixtureItems = TestBed.createComponent(AccordionItemComponent);
    componentItems = fixtureItems.componentInstance;
    componentItems.opened = false;  
    componentItems.title = 'tab1';  
    componentItems.toggle = new EventEmitter() ;  
    fixtureItems.detectChanges();

    fixture = TestBed.createComponent(AccordionComponent);
    component = fixture.componentInstance;
    component.items = new QueryList<AccordionItemComponent>()
    component.items.reset ([componentItems]) ;
  
    fixture.detectChanges();

  
  });

  it ('should Cread component',() =>{

    component.ngAfterContentInit();
    var openGroup = spyOn (component,'openGroup')
    component.openGroup(componentItems);
    expect(openGroup).toHaveBeenCalled();
    expect(component).toBeTruthy();
    expect(componentItems).toBeTruthy();

  });

   it ('should Create component',() =>{
    
    component.ngAfterContentInit();
    expect(component).toBeTruthy();
    expect(componentItems).toBeTruthy();

  });

  it ('should call openGroup', fakeAsync(() =>{
    const elms = fixtureItems.debugElement.query(By.css('.accordion__tab'));
    elms.triggerEventHandler('click', {});
    tick();
    fixture.detectChanges();
    var openGroup = spyOn (component,'openGroup')
    component.openGroup(componentItems);
    expect(openGroup).toHaveBeenCalled();
  }));

  // it('should activated the first one',() => {
  //   // spyOn(component, 'openGroup')
  //   // fixture.detectChanges();
  //   component.ngAfterContentInit();
  //   fixture.detectChanges();
  
  //   // const elms = fixture.debugElement.query(By.css(".accordion__tab"));
  //   // elms.nativeElement.click(component.items.toArray()[0]);
  //   // elms.nativeElement.triggerEventHandler('click', null);
  //   component.openGroup(component.items.toArray()[1])
  //   fixture.detectChanges();
  //   expect(component.items.toArray()[1].opened).toBeTruthy();
  // });
});
