import { Component, OnInit, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { AccordionItemComponent } from './accordion-item.component';

@Component({
  selector: 'app-accordion',
  template: '<div class="accordion"><ng-content></ng-content></div>',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements AfterContentInit {

  @ContentChildren(AccordionItemComponent) items:  QueryList<AccordionItemComponent>;

  constructor() {}

  ngAfterContentInit() {
  
    if (this.items.toArray()[0] !== undefined) {
      this.items.toArray()[0].opened = true;

      this.items.toArray().forEach((item) => {
        item.toggle.subscribe(() => {
          this.openGroup(item);
        });
      });
    }
  }

  openGroup(item) {
    

    this.items.toArray().forEach((t) => t.opened = false);
    item.opened = true;
  }

}
