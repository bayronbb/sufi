import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-accordion-item',
  templateUrl: './accordion-item.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionItemComponent implements OnInit {

    @Input() title: string;
    @Input() opened: Boolean;
    @Output() toggle: EventEmitter<any> = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

}