import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbModalContactComponent } from './ngb-modal-contact.component';

describe('NgbModalContactComponent', () => {
  let component: NgbModalContactComponent;
  let fixture: ComponentFixture<NgbModalContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgbModalContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgbModalContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
