import { Component, OnInit, Input } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'ngb-modal-contact',
  templateUrl: './ngb-modal-contact.component.html',
  styleUrls: ['./ngb-modal-contact.component.scss']
})
export class NgbModalContactComponent implements OnInit {

  @Input() image: string;
  @Input() alt:string;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }
}
