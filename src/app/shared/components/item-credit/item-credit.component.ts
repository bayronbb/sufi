import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item-credit',
  templateUrl: './item-credit.component.html'
})
export class ItemCreditComponent implements OnInit {

  @Input() image: string;
  @Input() alt: string;
  @Input() href: string;
  @Input() textLink: string;

  constructor() { }

  ngOnInit() {
  }

}
