import { SafePipe } from './safe.pipe';
import { DomSanitizer } from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';


describe('SafePipe', () => {
  let pipe: SafePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
            {
              provide: DomSanitizer,
              useValue: {
                  sanitize: () => 'unit&nsbp;test',
                  bypassSecurityTrustHtml: () => 'unit test'
              }
            }
        ]
    });

    let sanatizer = TestBed.get(DomSanitizer)
    pipe = new SafePipe(sanatizer);

  });

  it('Should create pipe', () => {
    expect(pipe).toBeTruthy();
  });

  it('Should return &nbsp;', () => {
    expect(pipe.transform("unit&nsbp;test")).toEqual("unit test");
  });
});