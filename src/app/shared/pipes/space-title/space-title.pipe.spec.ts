import { SpaceTitlePipe } from './space-title.pipe';
import { DomSanitizer } from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';

describe('SpaceTitlePipe', () => {
  let pipe: SpaceTitlePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DomSanitizer]
    });

    let sanatizer = TestBed.get(DomSanitizer)
    pipe = new SpaceTitlePipe(sanatizer);

  });

  it('Should return &nbsp;', () => {
    expect(pipe.transform("unit test")).toBe("<span>unit</span> <span>test</span>");
  });
});
