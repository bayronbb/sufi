import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'spaceTitle'
})
export class SpaceTitlePipe implements PipeTransform {

  constructor (private sanitizer: DomSanitizer) {
    
  }

  transform(value: string, ...args: any[]):string {
    let txtArr = value.split(' ');

    txtArr = txtArr.map( item => `<span>${ item }</span>`);
    
    let txtClean = txtArr.join(" ");
    //let newValue = value.replace(/ /g, "&nbsp;");
    return txtClean;
  }

}
