import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor () {}

  ngOnInit() {
    AOS.init({
      once: true,
      duration: 500,
      delay: 100
    });
  }
}
