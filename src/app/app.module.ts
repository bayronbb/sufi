import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';

//Module
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { MakeItModule } from './modules/make-it/make-it.module';
import { BlogModule } from './modules/blog/blog.module';
import { ExplorePossibilitiesModule } from './modules/explore-possibilities/explore-possibilities.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';

//Component
import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    MakeItModule,
    BlogModule,
    ExplorePossibilitiesModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPageScrollCoreModule.forRoot({duration: 800}),
  ],
  bootstrap: [AppComponent],
  providers: [{provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class AppModule { }
