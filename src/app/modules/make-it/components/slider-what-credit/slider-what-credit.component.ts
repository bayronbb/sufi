import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import Swiper, { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-slider-what-credit',
  templateUrl: './slider-what-credit.component.html',
  styleUrls: ['./slider-what-credit.component.scss']
})
export class SliderWhatCreditComponent implements AfterViewInit {

  public config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    slidesPerView: 1,
    spaceBetween: 30,
    autoHeight: true
  };

  swiper: Swiper;

  constructor() {
  }
  ngAfterViewInit() {
    this.swiper = new Swiper('.swiper-what-credit', {
      pagination: { el: '.swiper-pagination', clickable: true },
      slidesPerView: 1,
      spaceBetween: 30,
      autoHeight: true
    });
  }


}
