import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// Providers
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';

// Modules
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { MakeItRoutingModule } from 'src/app/modules/make-it/make-it-routing.module';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { SimplebarAngularModule } from 'simplebar-angular';
import { IMaskModule } from 'angular-imask';

// Pages
import { HomeComponent } from '../../pages/home/home.component';
import { ProductComponent } from 'src/app/modules/make-it//pages/product/product.component';
import { SimulatorComponent } from '../../pages/simulator/simulator.component';
import { ResultOneComponent } from '../../pages/result-one/result-one.component';
import { ResultTwoComponent } from '../../pages/result-two/result-two.component';
import { LetsCallComponent } from '../../pages/lets-call/lets-call.component';
import { LetsCallSentComponent } from '../../pages/lets-call-sent/lets-call-sent.component';

// Components
import { BannerProductComponent } from 'src/app/modules/make-it/components/banner-product/banner-product.component';
import { SectAchieveComponent } from 'src/app/modules/make-it//components/sect-achieve/sect-achieve.component';
import { StripInfoComponent } from 'src/app/modules/make-it//components/strip-info/strip-info.component';
import { WhatCreditComponent } from 'src/app/modules/make-it//components/what-credit/what-credit.component';
import { SectWaitComponent } from 'src/app/modules/make-it//components/sect-wait/sect-wait.component';
import { SliderWhatCreditComponent } from 'src/app/modules/make-it//components/slider-what-credit/slider-what-credit.component';
import { SliderWhatWantComponent } from 'src/app/modules/make-it//components/slider-what-want/slider-what-want.component';
import { InfoExpandibleComponent } from '../info-expandible/info-expandible.component';
import { SliderWaitComponent } from '../slider-wait/slider-wait.component';
import { QuestionButtonComponent } from '../question-button/question-button.component';
import { WhatWantComponent } from '../what-want/what-want.component';


describe('BannerProductComponent', () => {
  let component: BannerProductComponent;
  let fixture: ComponentFixture<BannerProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [    
        ProductComponent, 
        HomeComponent,
        SimulatorComponent,
        ResultOneComponent,
        ResultTwoComponent,
        LetsCallSentComponent,
        BannerProductComponent, 
        SectAchieveComponent, 
        StripInfoComponent, 
        WhatCreditComponent, 
        SectWaitComponent, 
        SliderWhatCreditComponent, SliderWhatWantComponent , 
        InfoExpandibleComponent, SliderWaitComponent,
        LetsCallComponent,
        QuestionButtonComponent,
        WhatWantComponent
      ],
      imports:[
        CommonModule,
        SharedModule,
        NgxPageScrollCoreModule.forRoot({duration: 800}),
        MakeItRoutingModule,
        SimplebarAngularModule,
        IMaskModule
      ],
     providers:[
        WindowRef
      ]


    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerProductComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
