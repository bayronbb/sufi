import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-banner-product',
  templateUrl: './banner-product.component.html',
  styleUrls: ['./banner-product.component.scss']
})
export class BannerProductComponent implements OnInit {

  @Input() title: String;
  @Input() image: String;

  constructor() { }

  ngOnInit() {
  }

}
