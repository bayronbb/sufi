import { Component, OnInit, HostListener, Output, EventEmitter } from '@angular/core';
import { Responsive } from 'src/app/configs/responsive';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';

@Component({
  selector: 'app-info-expandible',
  templateUrl: './info-expandible.component.html',
  styleUrls: ['./info-expandible.component.scss']
})
export class InfoExpandibleComponent implements OnInit {

  public tablet: Boolean = false;
  public responsive: Responsive;

  @Output() expandible: EventEmitter<Boolean>;

  constructor( private win: WindowRef ) { 
    let widthWin = this.win.nativeWindow.innerWidth;
    this.responsive = new Responsive();
    this.tablet = this.responsive.mqMin(widthWin, 768);
    this.expandible = new EventEmitter();
  }

  @HostListener('window:resize', [])

  ngOnInit() {
  }

  closeExpandible (e) {
    e.preventDefault();
    this.expandible.emit(false);
  }

  onResize(event:any):void {
    this.tablet = this.responsive.mqMin(event.target.innerWidth, 768);
	}

}
