import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

// MOdules
import { SharedModule } from 'src/app/shared/shared.module';
//Components
import { InfoExpandibleComponent } from './info-expandible.component';

//Providers
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';


describe('InfoExpandibleComponent', () => {
  let component: InfoExpandibleComponent;
  let fixture: ComponentFixture<InfoExpandibleComponent>;
  let closeElm: DebugElement;

  let event = {
    target: {
      innerWidth: 768
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        InfoExpandibleComponent
      ],
      imports: [
        SharedModule
      ],
      providers: [WindowRef]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoExpandibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Close expandible', () => {
    closeElm = fixture.debugElement.query(By.css('.info-expandable__close'));
    closeElm.nativeElement.click();
    fixture.detectChanges();
    expect(component.expandible.emit(false)).toBeFalsy();
  });

  it('Tablet should be true', () => {
    component.ngOnInit()
    fixture.detectChanges();
    component.onResize(event);
    fixture.detectChanges();
    //component.desktop = responsive.mqMin(event.target.innerWidth, 768);
    expect(component.tablet).toBeTruthy();
  });
});
