import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { Responsive } from 'src/app/configs/responsive';
import { DOCUMENT } from '@angular/common';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { PageScrollService } from 'ngx-page-scroll-core';

@Component({
  selector: 'app-what-want',
  templateUrl: './what-want.component.html'
})
export class WhatWantComponent implements OnInit {

  public xs_large: Boolean = false;
  public tablet: Boolean = false;
  public responsive: Responsive;
  public expandible: Boolean = false;

  constructor(
    private modalService: NgbModal, 
    private win: WindowRef, 
    private pageScroll: PageScrollService,
    @Inject(DOCUMENT) private document: any
  ) { 

    let widthWin = this.win.nativeWindow.innerWidth;
    this.responsive = new Responsive();
    this.xs_large = this.responsive.mqMin(widthWin, 1200);
    this.tablet = this.responsive.mqMin(widthWin, 768);

  }

  @HostListener('window:resize', [])

  ngOnInit() {
  }

  showExpandible(e) {
    e.preventDefault();
    this.expandible = !this.expandible;
    this.pageScroll.scroll({
      document: this.document,
      scrollTarget: '#what-want',
    });
    return false;
  }
  closeExpandible(e) {
    this.expandible = e;
  }

  onResize(event:any):void {
    this.xs_large = this.responsive.mqMin(event.target.innerWidth, 1200);
    this.tablet = this.responsive.mqMin(event.target.innerWidth, 768);
  }
  
  open(content:any) {
    console.log(content);
    this.modalService.open(content);
    return false;
  }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }
}
