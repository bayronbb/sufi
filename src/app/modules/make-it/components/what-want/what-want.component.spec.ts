import { async, ComponentFixture, TestBed, inject, tick, fakeAsync } from '@angular/core/testing';

// Components
import { WhatWantComponent } from './what-want.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SliderWhatWantComponent } from '../slider-what-want/slider-what-want.component';
import { InfoExpandibleComponent } from '../info-expandible/info-expandible.component';

// Providers
import { PageScrollService, NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { NgbModal, ModalDismissReasons, NgbModule, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';
import { By } from '@angular/platform-browser';

// Mock class for NgbModalRef
export class MockNgbModalRef {
  result: Promise<any> = new Promise((resolve, reject) => resolve('x'));
}
describe('WhatWantComponent', () => {
  let component: WhatWantComponent;
  let fixture: ComponentFixture<WhatWantComponent>;
  let ngbModal: NgbModal;
  let mockModalRef: MockNgbModalRef = new MockNgbModalRef();
  let modal = {
    dismiss (reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return `with: ${reason}`;
      }
    }
  }

  let event = {
    target: {
      innerWidth: 768
    },
    preventDefault() {
      return;
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        WhatWantComponent, 
        SliderWhatWantComponent,
        InfoExpandibleComponent,
      ],
      imports: [
        SharedModule,
        NgbModule,
        NgxPageScrollCoreModule.forRoot({duration: 800}),
      ],
      providers: [
        WindowRef,
        PageScrollService
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatWantComponent);
    component = fixture.componentInstance;
    ngbModal = TestBed.get(NgbModal);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should show InfoExpandible', () => {
    component.ngOnInit();
    component.showExpandible(event);
    fixture.detectChanges();
    expect(component.expandible).toBeTruthy();
  });

  it('Should close InfoExpandible', () => {
    component.ngOnInit();
    component.showExpandible(event);
    fixture.detectChanges();
    component.closeExpandible(false);
    fixture.detectChanges();
    expect(component.expandible).toBeFalsy();
  });

  it('Tablet should be true', () => {
    component.ngOnInit()
    fixture.detectChanges();
    component.onResize(event);
    fixture.detectChanges();
    expect(component.tablet).toBeTruthy();
  });

  it('should open modal', () => {
      spyOn(ngbModal, 'open').and.returnValue(mockModalRef as any);
      fixture.detectChanges();
      component.open('<xxxx>');
      expect(ngbModal.open).toHaveBeenCalledWith("<xxxx>");
  });
});
