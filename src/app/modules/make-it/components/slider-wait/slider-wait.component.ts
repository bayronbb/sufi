import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import Swiper, { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-slider-wait',
  templateUrl: './slider-wait.component.html',
  styleUrls: ['./slider-wait.component.scss']
})
export class SliderWaitComponent implements AfterViewInit {

  public config: SwiperOptions = {
    slidesPerView: 'auto',
    centeredSlides: true,
    spaceBetween: 50,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  };

  swiper: Swiper;

  constructor() {
  }
  ngAfterViewInit() {
    this.swiper = new Swiper('.swiper-container--wait', {
        slidesPerView: 1.2,
        spaceBetween: 30,
        centeredSlides: true,
        initialSlide: 1,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
    });
  }

}
