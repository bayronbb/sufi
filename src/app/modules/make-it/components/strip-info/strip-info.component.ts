import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-strip-info',
  templateUrl: './strip-info.component.html',
  styleUrls: ['./strip-info.component.scss']
})
export class StripInfoComponent implements OnInit {

  @Input() title: String;

  constructor() { }

  ngOnInit() {
  }

}
