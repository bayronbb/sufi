import { Component, OnInit, HostListener } from '@angular/core';
import { Responsive } from 'src/app/configs/responsive';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';

@Component({
  selector: 'app-what-credit',
  templateUrl: './what-credit.component.html',
  styleUrls: ['./what-credit.component.scss']
})
export class WhatCreditComponent implements OnInit {

  public desktop: Boolean = false;
  public responsive: Responsive;

  constructor( private win: WindowRef ) { 
    let widthWin = this.win.nativeWindow.innerWidth;
    this.responsive = new Responsive();
    this.desktop = this.responsive.mqMin(widthWin, 768);
  }

  @HostListener('window:resize', [])

  ngOnInit() {
  }

  onResize(event:any):void {
    this.desktop = this.responsive.mqMin(event.target.innerWidth, 768);
	}
}
