import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';

import { SectWaitComponent } from './sect-wait.component';
import { SliderWaitComponent } from '../slider-wait/slider-wait.component';
import { Responsive } from 'src/app/configs/responsive';

describe('InfoExpandibleComponent', () => {
    let component: SectWaitComponent;
    let fixture: ComponentFixture<SectWaitComponent>;

    let event = {
        target: {
            innerWidth: 768
        }
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                SectWaitComponent,
                SliderWaitComponent
            ],
            providers: [WindowRef]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SectWaitComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

    });

    it('Tablet should be true', () => {
        component.ngOnInit()
        fixture.detectChanges();
        component.onResize(event);
        fixture.detectChanges();
        //component.desktop = responsive.mqMin(event.target.innerWidth, 768);
        expect(component.medium).toBeTruthy();
    });
})