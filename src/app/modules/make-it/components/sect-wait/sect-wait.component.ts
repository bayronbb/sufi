import { Component, OnInit, HostListener } from '@angular/core';
import { Responsive } from 'src/app/configs/responsive';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';

@Component({
  selector: 'app-sect-wait',
  templateUrl: './sect-wait.component.html',
  styleUrls: ['./sect-wait.component.scss']
})
export class SectWaitComponent implements OnInit {

  public medium: Boolean = false;
  public responsive: Responsive;

  constructor( private win: WindowRef ) {
    let widthWin = this.win.nativeWindow.innerWidth;
    this.responsive = new Responsive();
    this.medium = this.responsive.mqMin(widthWin, 768);
  }

  @HostListener('window:resize', [])

  ngOnInit() {
  }

  onResize(event:any):void {
    this.medium = this.responsive.mqMin(event.target.innerWidth, 768);
	}

}
