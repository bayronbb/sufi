import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question-button',
  templateUrl: './question-button.component.html',
  styleUrls: ['./question-button.component.scss']
})
export class QuestionButtonComponent implements OnInit {
  @Input() href: string;
  @Input() styleClass: string;

  constructor() { }

  ngOnInit() {
  }

}
