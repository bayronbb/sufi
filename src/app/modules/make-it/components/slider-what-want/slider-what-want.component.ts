import { Component, OnInit, AfterViewInit } from '@angular/core';
import Swiper, { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-slider-what-want',
  templateUrl: './slider-what-want.component.html',
  styleUrls: ['./slider-what-want.component.scss']
})
export class SliderWhatWantComponent implements AfterViewInit {

  public config: Object = {
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // },
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 1,
    spaceBetween: 40,
    autoHeight: true
  };

  swiper: Swiper;

  constructor() {
  }
  ngAfterViewInit() {
    this.swiper = new Swiper('.swiper-what-want', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      slidesPerView: 1,
    });
  }
}
