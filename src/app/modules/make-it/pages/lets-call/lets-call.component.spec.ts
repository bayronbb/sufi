import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { SharedModule } from 'src/app/shared/shared.module';
import { IMaskModule } from 'angular-imask';
import { NgxPageScrollCoreModule, PageScrollService } from 'ngx-page-scroll-core';

import { LetsCallComponent } from './lets-call.component';
import { SectWaitComponent } from '../../components/sect-wait/sect-wait.component';
import { SliderWaitComponent } from '../../components/slider-wait/slider-wait.component';

// Provioder
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';

describe('LetsCallComponent', () => {
  let component: LetsCallComponent;
  let fixture: ComponentFixture<LetsCallComponent>;
  let testBedService: PageScrollService;
  let event = {
    preventDefault() {
      return;
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        LetsCallComponent, 
        SectWaitComponent,
        SliderWaitComponent
      ],
      imports: [
        SharedModule,
        IMaskModule,
        NgxPageScrollCoreModule
      ],
      providers: [
        WindowRef,
        PageScrollService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LetsCallComponent);
    component = fixture.componentInstance;
    testBedService = TestBed.get(PageScrollService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("Should move to the form", inject([PageScrollService], (injectService: PageScrollService) => {
    component.ngOnInit();
    component.goToForm(event);
    fixture.detectChanges();
    expect(injectService).toBe(testBedService);
  }))
});
