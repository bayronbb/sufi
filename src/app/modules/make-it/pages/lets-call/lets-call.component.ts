import { Component, OnInit, Inject } from '@angular/core';
import { PageScrollService } from 'ngx-page-scroll-core';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-lets-call',
  templateUrl: './lets-call.component.html',
  styleUrls: ['./lets-call.component.scss']
})
export class LetsCallComponent implements OnInit {

  public model: String;
  public placement = 'bottom';
  public currentDate = new Date();
  public numberMask = {
    mask: Number,
    min: 1,
    thousandsSeparator: ' '
  };
  public dayMask = {
    mask: Date,
    from: 1,
    to: 31,
    maxLength: 2,
  };
  public yearMask = {
    mask: Number,
    min: 1900,
    max: this.currentDate.getFullYear(),
  };


  constructor(private pageScroll: PageScrollService, @Inject(DOCUMENT) private document: any) { }

  ngOnInit() {
  }

  goToForm(e) {
    e.preventDefault();
    this.pageScroll.scroll({
      document: this.document,
      scrollTarget: '.sect-simulator__cont',
    });
  }

}
