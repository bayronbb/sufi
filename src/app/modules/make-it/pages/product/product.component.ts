import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';
import { Responsive } from 'src/app/configs/responsive';
import { PageScrollService } from 'ngx-page-scroll-core';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public xs_large: Boolean = false;
  public tablet: Boolean = false;
  public responsive: Responsive;
  public expandible: Boolean = false;

  constructor( private win: WindowRef, private pageScroll: PageScrollService, @Inject(DOCUMENT) private document: any ) {
    let widthWin = this.win.nativeWindow.innerWidth;
    this.responsive = new Responsive();
    this.xs_large = this.responsive.mqMin(widthWin, 1200);
    this.tablet = this.responsive.mqMin(widthWin, 768);
  }

  @HostListener('window:resize', [])

  ngOnInit() {
  }

  showExpandible(e) {
    e.preventDefault();
    this.expandible = !this.expandible;
    this.pageScroll.scroll({
      document: this.document,
      scrollTarget: '#what-want',
    });
    return false;
  }
  closeExpandible(e) {
    this.expandible = e;
  }

  onResize(event:any):void {
    this.xs_large = this.responsive.mqMin(event.target.innerWidth, 1200);
    this.tablet = this.responsive.mqMin(event.target.innerWidth, 768);
	}

}
