import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// Modules
import { SharedModule } from 'src/app/shared/shared.module';

// Pages
import { ResultOneComponent } from './result-one.component';

// Components
import { QuestionButtonComponent } from '../../components/question-button/question-button.component';
import { WhatWantComponent } from '../../components/what-want/what-want.component';
import { SectWaitComponent } from '../../components/sect-wait/sect-wait.component';
import { SliderWhatWantComponent } from '../../components/slider-what-want/slider-what-want.component';
import { InfoExpandibleComponent } from '../../components/info-expandible/info-expandible.component';
import { SliderWaitComponent } from '../../components/slider-wait/slider-wait.component';

// Providers
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';
import { PageScrollService, NgxPageScrollCoreModule } from 'ngx-page-scroll-core';

describe('SimulatorStepThreeComponent', () => {
  let component: ResultOneComponent;
  let fixture: ComponentFixture<ResultOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ResultOneComponent, 
        QuestionButtonComponent, 
        WhatWantComponent,
        SectWaitComponent,
        SliderWhatWantComponent,
        InfoExpandibleComponent,
        SliderWaitComponent,
      ],
      imports: [
        SharedModule,
        NgxPageScrollCoreModule.forRoot({duration: 800}),
      ],
      providers: [
        WindowRef,
        PageScrollService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
