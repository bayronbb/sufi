import { Component, OnInit, Inject } from '@angular/core';
import { PageScrollService } from 'ngx-page-scroll-core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-simulator',
  templateUrl: './simulator.component.html',
  styleUrls: ['./simulator.component.scss']
})
export class SimulatorComponent implements OnInit {

  public tabSelect: string;

  public costMask = {
    mask: '$num',
    blocks: {
      num: {
        mask: Number,
        min: 300000,
        max: 21945075,
        thousandsSeparator: '.',
      }
    }
  };

  constructor(private pageScroll: PageScrollService, @Inject(DOCUMENT) private document: any) { 
    this.tabSelect = 'pregrado';
  }

  ngOnInit() {
  }

  selectTypeCredit (e) {
    e.preventDefault();
    this.tabSelect = e.target.dataset.credit;
    this.pageScroll.scroll({
      document: this.document,
      scrollTarget: '.step-content',
    });
  }

}
