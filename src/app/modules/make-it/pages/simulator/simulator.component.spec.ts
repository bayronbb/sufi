import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// Module
import { SharedModule } from 'src/app/shared/shared.module';
import { IMaskModule } from 'angular-imask';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';

// Pages
import { SimulatorComponent } from './simulator.component';

// Components
import { QuestionButtonComponent } from '../../components/question-button/question-button.component';

import { ElementRef } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('SimulatorComponent', () => {
  let component: SimulatorComponent;
  let fixture: ComponentFixture<SimulatorComponent>;
  let elm: ElementRef;
  let e = {
    preventDefault: function () {}
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        SimulatorComponent,
        QuestionButtonComponent
      ],
      imports: [
        SharedModule,
        IMaskModule,
        NgxPageScrollCoreModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("Should choose pregrados", () => {
    component.ngOnInit();
    fixture.detectChanges();
    elm = fixture.debugElement.query(By.css(".btn-option"));
    elm.nativeElement.click(e);
    expect(component.tabSelect).toBe("pregrado");
  })
});
