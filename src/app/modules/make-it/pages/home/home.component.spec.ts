import { async, ComponentFixture, TestBed } from '@angular/core/testing';

//Modules
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { MakeItRoutingModule } from '../../make-it-routing.module';
import { SimplebarAngularModule } from 'simplebar-angular';
import { IMaskModule } from 'angular-imask';

// Pages
import { HomeComponent } from '../home/home.component';
import { ProductComponent } from '../product/product.component';
import { SimulatorComponent } from '../simulator/simulator.component';
import { ResultOneComponent } from '../result-one/result-one.component';
import { ResultTwoComponent } from '../result-two/result-two.component';
import { LetsCallComponent } from '../lets-call/lets-call.component';
import { LetsCallSentComponent } from '../lets-call-sent/lets-call-sent.component';

//Components
import { BannerProductComponent } from '../../components/banner-product/banner-product.component';
import { SectAchieveComponent } from '../../components/sect-achieve/sect-achieve.component';
import { StripInfoComponent } from '../../components/strip-info/strip-info.component';
import { WhatCreditComponent } from '../../components/what-credit/what-credit.component';
import { SliderWhatWantComponent } from '../../components/slider-what-want/slider-what-want.component';
import { InfoExpandibleComponent } from '../../components/info-expandible/info-expandible.component';
import { SectWaitComponent } from '../../components/sect-wait/sect-wait.component';
import { SliderWhatCreditComponent } from '../../components/slider-what-credit/slider-what-credit.component';
import { SliderWaitComponent } from '../../components/slider-wait/slider-wait.component';

// Providers
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';
import { QuestionButtonComponent } from '../../components/question-button/question-button.component';
import { WhatWantComponent } from '../../components/what-want/what-want.component';


describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ProductComponent, 
        HomeComponent,
        SimulatorComponent,
        ResultOneComponent,
        ResultTwoComponent,
        LetsCallComponent,
        LetsCallSentComponent,
        BannerProductComponent, 
        SectAchieveComponent, 
        StripInfoComponent, 
        WhatCreditComponent, 
        SectWaitComponent, 
        SliderWhatCreditComponent, 
        SliderWhatWantComponent,
        SliderWaitComponent, 
        InfoExpandibleComponent, 
        SliderWaitComponent,
        QuestionButtonComponent,
        WhatWantComponent,
      ],
      imports:[
        CommonModule,
        SharedModule,
        NgxPageScrollCoreModule.forRoot({duration: 800}),
        MakeItRoutingModule,
        SimplebarAngularModule,
        IMaskModule
      ],
     providers:[
        WindowRef
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
