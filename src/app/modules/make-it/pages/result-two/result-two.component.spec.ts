import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

// Module
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SimplebarAngularModule } from 'simplebar-angular';

// Components
import { ResultTwoComponent } from './result-two.component';
import { QuestionButtonComponent } from '../../components/question-button/question-button.component';
import { WhatWantComponent } from '../../components/what-want/what-want.component';
import { SectWaitComponent } from '../../components/sect-wait/sect-wait.component';
import { SliderWhatWantComponent } from '../../components/slider-what-want/slider-what-want.component';
import { InfoExpandibleComponent } from '../../components/info-expandible/info-expandible.component';
import { SliderWaitComponent } from '../../components/slider-wait/slider-wait.component';

// Providers
import { WindowRef } from 'src/app/core/providers/window-ref/window-ref';
import { PageScrollService, NgxPageScrollCoreModule } from 'ngx-page-scroll-core';

export class MockNgbModalRef {
  result: Promise<any> = new Promise((resolve, reject) => resolve('x'));
}

describe('SimulatorComponent', () => {
  let component: ResultTwoComponent;
  let fixture: ComponentFixture<ResultTwoComponent>;
  let ngbModal: NgbModal;
  let mockModalRef: MockNgbModalRef = new MockNgbModalRef();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ResultTwoComponent,
        QuestionButtonComponent,
        WhatWantComponent,
        SectWaitComponent,
        SliderWhatWantComponent,
        InfoExpandibleComponent,
        SliderWaitComponent,
      ],
      imports: [
        SharedModule,
        NgbModule,
        NgxPageScrollCoreModule.forRoot({ duration: 800 }),
        SimplebarAngularModule,
      ],
      providers: [
        WindowRef,
        PageScrollService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultTwoComponent);
    component = fixture.componentInstance;
    ngbModal = TestBed.get(NgbModal);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal', () => {
    spyOn(ngbModal, 'open').and.returnValue(mockModalRef as any);
    fixture.detectChanges();
    component.open('<xxxx>');
    expect(ngbModal.open).toHaveBeenCalledWith("<xxxx>");
  });

});
