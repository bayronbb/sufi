import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LetsCallSentComponent } from './lets-call-sent.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('LetsCallSentComponent', () => {
  let component: LetsCallSentComponent;
  let fixture: ComponentFixture<LetsCallSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LetsCallSentComponent ],
      imports: [SharedModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LetsCallSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
