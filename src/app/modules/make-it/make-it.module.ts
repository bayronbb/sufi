import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MakeItRoutingModule } from './make-it-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SimplebarAngularModule } from 'simplebar-angular';
import { IMaskModule } from 'angular-imask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//Pages
import { HomeComponent } from './pages/home/home.component';
import { ProductComponent } from './pages/product/product.component';
import { SimulatorComponent } from './pages/simulator/simulator.component';
import { ResultTwoComponent } from './pages/result-two/result-two.component';
import { ResultOneComponent } from './pages/result-one/result-one.component';
import { LetsCallComponent } from './pages/lets-call/lets-call.component';
import { LetsCallSentComponent } from './pages/lets-call-sent/lets-call-sent.component';

//Components
import { BannerProductComponent } from './components/banner-product/banner-product.component';
import { SectAchieveComponent } from './components/sect-achieve/sect-achieve.component';
import { StripInfoComponent } from './components/strip-info/strip-info.component';
import { WhatCreditComponent } from './components/what-credit/what-credit.component';
import { SectWaitComponent } from './components/sect-wait/sect-wait.component';
import { SliderWhatCreditComponent } from './components/slider-what-credit/slider-what-credit.component';
import { SliderWhatWantComponent } from './components/slider-what-want/slider-what-want.component';
import { SliderWaitComponent } from './components/slider-wait/slider-wait.component';
import { InfoExpandibleComponent } from './components/info-expandible/info-expandible.component';
import { QuestionButtonComponent } from './components/question-button/question-button.component';
import { WhatWantComponent } from './components/what-want/what-want.component';
import { SimulatorHomeComponent } from './pages/simulator-home/simulator-home.component';

@NgModule({
  declarations: [
    ProductComponent, 
    HomeComponent, 
    ResultTwoComponent,
    BannerProductComponent, 
    SectAchieveComponent, 
    StripInfoComponent, 
    WhatCreditComponent, 
    SectWaitComponent, 
    SliderWhatCreditComponent, 
    SliderWhatWantComponent, 
    SliderWaitComponent, 
    InfoExpandibleComponent, 
    QuestionButtonComponent, 
    SimulatorComponent,
    ResultOneComponent,
    WhatWantComponent,
    LetsCallComponent,
    LetsCallSentComponent,
    SimulatorHomeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MakeItRoutingModule,
    SimplebarAngularModule,
    FormsModule,
    NgbModule,
    IMaskModule
  ]
})
export class MakeItModule { }
