import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';

//Components
import { ProductComponent } from './pages/product/product.component';
import { HomeComponent } from './pages/home/home.component';
import { SimulatorHomeComponent } from './pages/simulator-home/simulator-home.component';
import { SimulatorComponent } from './pages/simulator/simulator.component';
import { ResultTwoComponent } from './pages/result-two/result-two.component';
import { ResultOneComponent } from './pages/result-one/result-one.component';
import { LetsCallComponent } from './pages/lets-call/lets-call.component';
import { LetsCallSentComponent } from './pages/lets-call-sent/lets-call-sent.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'producto',
    component: ProductComponent
  },
  {
    path: 'simulador-home',
    component: SimulatorHomeComponent
  },
  {
    path: 'simulador',
    component: SimulatorComponent
  },
  {
    path: 'resultado-1',
    component: ResultOneComponent
  },
  {
    path: 'resultado-2',
    component: ResultTwoComponent
  },
  {
    path: 'dejanos-llamarte',
    component: LetsCallComponent
  },
  {
    path: 'dejanos-llamarte-enviado',
    component: LetsCallSentComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [{provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class MakeItRoutingModule { }
